<?php
$CONFIG = array (
  'passwordsalt' => 'gnL4FnObnXvDfVlcbdOXt53yuTFkdT',
  'secret' => '3tDhoPdRlIdlZypoecDyHdnxXtIPDt9UEhJhk3syJgsPC1ck',
  'trusted_domains' => 
  array (
    0 => 'localhost',
    1 => 'owncloud.localdomain:8082',
  ),
  'datadirectory' => '/var/www/html/data',
  'overwrite.cli.url' => 'http://localhost',
  'dbtype' => 'mysql',
  'version' => '9.1.0.15',
  'dbname' => 'owncloud_db',
  'dbhost' => 'localhost',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'owncloud',
  'dbpassword' => 'owncloud',
  'logtimezone' => 'UTC',
  'installed' => true,
  'instanceid' => 'oc09o2rl25fj',
  'ldapIgnoreNamingRules' => false,
);
