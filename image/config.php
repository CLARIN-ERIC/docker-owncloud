<?php
$CONFIG = array (
  'passwordsalt' => 'OwroM3vSpD2HRSujjK3c2DLoOUgrvb',
  'secret' => 'BSUO8i/fvUpk0wZSuQuYUYmYum9t83Mw46BJKppTArnhzfVb',
  'trusted_domains' => 
  array (
    0 => 'localhost',
  ),
  'datadirectory' => '/var/www/html/data',
  'overwrite.cli.url' => 'http://localhost',
  'dbtype' => 'mysql',
  'version' => '9.1.0.15',
  'dbname' => 'owncloud_db',
  'dbhost' => 'localhost',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'owncloud',
  'dbpassword' => 'owncloud',
  'logtimezone' => 'UTC',
  'installed' => true,
  'instanceid' => 'ocyfph8vtak2',
  'ldapIgnoreNamingRules' => false,
);
