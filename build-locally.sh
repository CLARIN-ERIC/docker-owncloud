#!/bin/sh

set -e

docker run \
    --volume='/var/run/docker.sock:/var/run/docker.sock' \
    --rm \
    --volume="$PWD":"$PWD" \
    --workdir="$PWD" \
    -it \
    docker:1.11.2 \
    sh -c "sh -x ./build.sh && sh -x ./test.sh"
