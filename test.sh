#!/bin/sh

set -e

apk --quiet update --update-cache
apk --quiet add 'py-pip==8.1.2-r0'
pip --quiet --disable-pip-version-check install 'docker-compose==1.8.0'

. ./env.sh
docker load --input="$IMAGE_FILE_PATH"
docker-compose -f 'run/docker-compose.yml' run baseimage
number_of_failed_containers="$(docker-compose ps -q | xargs docker inspect \
    -f '{{ .State.ExitCode }}' | grep -c 0 -v | tr -d ' ')"
exit "$number_of_failed_containers"